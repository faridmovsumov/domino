<?php

namespace Test;

use App\DominoGameEngine;
use App\Player;
use PHPUnit\Framework\TestCase;

class DominoGameEngineTest extends TestCase
{
    public function testGameInitAndFirstProgress()
    {
        $alice = new Player("Alice");
        $bob = new Player("Bob");

        $this->assertInstanceOf("App\\Player", $alice);
        $this->assertEquals("Alice", $alice->getName());

        $players = [
            $alice,
            $bob
        ];

        //Test Initial Setup
        $dominoGameEngine = new DominoGameEngine($players);
        $this->assertEquals($players, $dominoGameEngine->getPlayers());
        $this->assertFalse($dominoGameEngine->isGameFinished());
        $this->assertInstanceOf("App\\Tile", $dominoGameEngine->getFirstTile());
        $this->assertEquals("Alice", $dominoGameEngine->getActivePlayer()->getName());
        $this->assertFalse($dominoGameEngine->isGameFinished());
        $this->assertEquals("Game starting with first tile: " . $dominoGameEngine->getFirstTile(), $dominoGameEngine->getBoardStatus());

        //Test Game progress
        $player = $dominoGameEngine->getActivePlayer();
        $dominoGameEngine->progressMove();
        $playerName = $player->getName();
        $lastPlayedTileByPlayer = $player->getLastPlayedTile();
        $lastConnectedTile = $player->getLastConnectedTile();
        $this->assertEquals("$playerName plays $lastPlayedTileByPlayer to connect to tile $lastConnectedTile on the board", $dominoGameEngine->getLastPlayerEvent());
        $this->stringStartsWith("Board is now: <", $dominoGameEngine->getBoardStatus());
        $this->assertFalse($dominoGameEngine->isGameFinished());
    }
}