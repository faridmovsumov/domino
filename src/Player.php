<?php

namespace App;

class Player
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var array
     */
    private $tiles = [];

    /**
     * @var Tile
     */
    private $lastPlayedTile;

    /**
     * @var Tile
     */
    private $lastConnectedTile;

    /**
     * @var bool
     */
    private $noMoreMoves = false;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function addTile(Tile $tile): void
    {
        $this->tiles[] = $tile;
    }

    /**
     * @return array
     */
    public function getTiles(): array
    {
        return $this->tiles;
    }

    /**
     * @return Tile
     */
    public function getLastPlayedTile(): Tile
    {
        return $this->lastPlayedTile;
    }

    /**
     * @param Tile $lastPlayedTile
     */
    public function setLastPlayedTile(Tile $lastPlayedTile): void
    {
        $this->lastPlayedTile = $lastPlayedTile;

        if (($key = array_search($lastPlayedTile, $this->tiles)) !== false) {
            unset($this->tiles[$key]);
        }
    }

    /**
     * @return Tile
     */
    public function getLastConnectedTile(): Tile
    {
        return $this->lastConnectedTile;
    }

    /**
     * @param Tile $lastConnectedTile
     */
    public function setLastConnectedTile(Tile $lastConnectedTile): void
    {
        $this->lastConnectedTile = $lastConnectedTile;
    }

    /**
     * @return bool
     */
    public function hasNoMoreMoves(): bool
    {
        return $this->noMoreMoves;
    }

    /**
     * @param bool $noMoreMoves
     */
    public function setNoMoreMoves(bool $noMoreMoves): void
    {
        $this->noMoreMoves = $noMoreMoves;
    }
}