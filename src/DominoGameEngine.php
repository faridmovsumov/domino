<?php

namespace App;

class DominoGameEngine
{
    /**
     * @var array
     */
    private $players;

    /**
     * @var array
     */
    private $tiles;

    /**
     * @var Tile
     */
    private $firstTile;

    /**
     * @var int
     */
    private $activePlayerIndex = 0;

    /**
     * @var array
     */
    private $lineOfPlay = [];

    /**
     * @var bool
     */
    private $isGameFinished = false;

    /**
     * @var string
     */
    private $lastPlayerEvent;

    /**
     * @var Player
     */
    private $winner;

    /**
     * @var bool
     */
    private $hasNoMoreMove;

    public function __construct(array $players)
    {
        $this->setPlayers($players);
        $this->init();
    }

    private function init(): void
    {
        foreach (range(0, 6) as $firstNumber) {
            for ($secondNumber = 0; $secondNumber <= $firstNumber; $secondNumber++) {
                $tile = new Tile($firstNumber, $secondNumber);
                $this->tiles[] = $tile;
            }
        }

        $this->chooseFirstTile();
        $this->givePlayersInitialTiles();
    }

    /**
     * @return array
     */
    public function getPlayers(): array
    {
        return $this->players;
    }

    /**
     * @param array $players
     */
    public function setPlayers(array $players): void
    {
        foreach ($players as $player) {
            if (!$player instanceof Player) {
                throw new \InvalidArgumentException("Invalid player object");
            }
        }
        $this->players = $players;
    }

    public function isGameFinished(): bool
    {
        return $this->isGameFinished;
    }

    public function getFirstTile(): Tile
    {
        return $this->firstTile;
    }

    public function getBoardStatus(): string
    {
        if ($this->isGameFinished) {
            $winnerName = $this->winner->getName();
            return "Player $winnerName has won!";
        }

        if (count($this->lineOfPlay) === 1) {
            return "Game starting with first tile: " . $this->getFirstTile();
        }

        return "Board is now: " . implode(" ", $this->lineOfPlay);
    }

    public function progressMove(): void
    {
        $activePlayer = $this->getActivePlayer();
        $tilesOfPlayer = $activePlayer->getTiles();
        $availableTile = $this->tryToPutTile($tilesOfPlayer);

        while ($availableTile === null) {

            if ($this->getTileCountInDeck() <= 0) {
                $this->hasNoMoreMove = true;
                $this->lastPlayerEvent = "Deck is empty " . $activePlayer->getName() . " doesn't have any move and lost";
                $this->switchActivePlayer(false);
                $this->setWinner($this->getActivePlayer());
                return;
            }

            $tileFromDeck = $this->getTileFromDeck();
            echo "\n" . $activePlayer->getName() . "... is fetching " . $tileFromDeck . " tile from deck\n";
            $availableTile = $this->tryToPutTile([$tileFromDeck]);
            if ($availableTile === null) {
                $activePlayer->addTile($tileFromDeck);
            }
        }

        if ($availableTile) {
            $activePlayer->setLastPlayedTile($availableTile);
            if (count($activePlayer->getTiles()) <= 0) {
                $this->setWinner($activePlayer);
                $this->setLastPlayerEvent($activePlayer);
                $this->lastPlayerEvent .= '. Congratulations!! ' . $activePlayer->getName() . ' finished all tiles.';
                return;
            } else {
                $this->switchActivePlayer();
                return;
            }
        }
    }

    private function setWinner(Player $player)
    {
        $this->isGameFinished = true;
        $this->winner = $player;
    }

    private function switchActivePlayer(bool $setLastPlayerEvent = true)
    {
        if ($setLastPlayerEvent === true) {
            $this->setLastPlayerEvent($this->getActivePlayer());
        }
        if ($this->activePlayerIndex === (count($this->players) - 1)) {
            $this->activePlayerIndex = 0;
        } else {
            $this->activePlayerIndex++;
        }
    }

    private function addTileToEnd(Tile $tile): void
    {
        array_push($this->lineOfPlay, $tile);
    }

    private function addTileToHead(Tile $tile): void
    {
        array_unshift($this->lineOfPlay, $tile);
    }

    private function getTileCountInDeck()
    {
        return count($this->tiles);
    }

    public function getActivePlayer(): Player
    {
        return $this->players[$this->activePlayerIndex];
    }

    private function chooseFirstTile(): void
    {
        shuffle($this->tiles);
        $this->firstTile = $this->getTileFromDeck();
        $this->lineOfPlay[] = $this->firstTile;
    }

    private function getTileFromDeck(): Tile
    {
        return array_shift($this->tiles);
    }

    private function givePlayersInitialTiles(): void
    {
        /** @var Player $player */
        foreach ($this->getPlayers() as $index => $player) {
            for ($i = 1; $i <= 7; $i++) {
                $player->addTile(array_shift($this->tiles));
            }
        }
    }

    private function getHeadTile(): Tile
    {
        return $this->lineOfPlay[0];
    }

    private function getEndTile(): Tile
    {
        return end($this->lineOfPlay);
    }

    /**
     * @param $tilesOfPlayer
     * @return Tile|null
     */
    private function tryToPutTile($tilesOfPlayer)
    {
        $headTile = $this->getHeadTile();
        $endTile = $this->getEndTile();

        $headNumber = $headTile->getHeadNumber();
        $tailNumber = $endTile->getTailNumber();

        $availableTile = null;
        $activePlayer = $this->getActivePlayer();

        /** @var Tile $tile */
        foreach ($tilesOfPlayer as $tile) {

            if ($tile->getTailNumber() === $headNumber) {
                $availableTile = $tile;
                $this->addTileToHead($tile);
                $activePlayer->setLastConnectedTile($headTile);
                break;
            } elseif ($tile->getHeadNumber() === $tailNumber) {
                $availableTile = $tile;
                $this->addTileToEnd($tile);
                $activePlayer->setLastConnectedTile($endTile);
                break;
            } elseif ($tile->getHeadNumber() === $headNumber) {
                $tile->flip();
                $availableTile = $tile;
                $this->addTileToHead($tile);
                $activePlayer->setLastConnectedTile($headTile);
                break;
            } elseif ($tile->getTailNumber() === $tailNumber) {
                $tile->flip();
                $availableTile = $tile;
                $this->addTileToEnd($tile);
                $activePlayer->setLastConnectedTile($endTile);
                break;
            }
        }

        return $availableTile;
    }

    private function setLastPlayerEvent(Player $player)
    {
        $playerName = $player->getName();
        $lastPlayedTileByPlayer = $player->getLastPlayedTile();
        $lastConnectedTile = $player->getLastConnectedTile();
        $this->lastPlayerEvent = "$playerName plays $lastPlayedTileByPlayer to connect to tile $lastConnectedTile on the board";
    }

    public function getLastPlayerEvent()
    {
        return $this->lastPlayerEvent;
    }
}