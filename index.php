<?php

declare(strict_types=1);

use App\Console;
use App\DominoGameEngine;
use App\Player;

require_once "vendor/autoload.php";

$alice = new Player("Alice");
$bob = new Player("Bob");

$players = [
    $alice,
    $bob
];

//Test Initial Setup
$dominoGameEngine = new DominoGameEngine($players);

Console::writeLine($dominoGameEngine->getBoardStatus());

while (!$dominoGameEngine->isGameFinished()) {
    $activePlayerName = $dominoGameEngine->getActivePlayer()->getName();
    Console::writeLine("Tiles of active player $activePlayerName : " . implode($dominoGameEngine->getActivePlayer()->getTiles()));
    $dominoGameEngine->progressMove();
    Console::writeLine($dominoGameEngine->getLastPlayerEvent());
    Console::writeLine($dominoGameEngine->getBoardStatus());
}

