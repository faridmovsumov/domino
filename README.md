Assignment
===============

**Author:** Farid Movsumov

## Installation

### Composer install

```bash
composer install
```

## How to run console application?

```bash
php index.php
```

## How to run tests?

```bash
vendor/bin/phpunit --colors  test/
```

## Notes

- I included vendor to make it easier to setup.